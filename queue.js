let collection = [];

// Write the queue functions below.

function print() {
    return collection;
    // It will show the array
}

function enqueue(element) {
    collection[collection.length] = element;
    return collection;
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
}

function dequeue() {
    for (let i = 1; i < collection.length; i++)
        collection[i - 1] = collection[i];
    collection.length--;
    return collection;
    // In here you are going to remove the last element in the array
}

function front() {
    const [firstElement, ...rest] = collection;
    return firstElement;
    // In here, you are going to get the first element
}

// starting from here, di na pwede gumamit ng .length property
function size() {
    let lengthOfCollection = 0;
    while (collection[lengthOfCollection]) lengthOfCollection++;
    return lengthOfCollection;

    // Number of elements
}

function isEmpty() {
    const checker = collection[0] == undefined;
    if (!checker) return false;

    //it will check whether the array is empty or not
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty,
};
